package faskteam.faskandroid.utilities.gps_location;

import android.location.Location;

/**
 * Created by Sam.I on 11/23/2015.
 */
public interface LocationCallback {
    void onReceiveLocation(Location location);
}

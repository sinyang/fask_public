package faskteam.faskandroid.utilities;

/**
 * Created by Sam.I on 11/24/2015.
 */
public interface MenuSearchListener {
    void onSearchSubmit(final String search);
}

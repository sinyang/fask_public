package faskteam.faskandroid.entities;

import java.util.HashMap;

/**
 * Created by Sam.I on 10/29/2015.
 */
public class FriendsList extends Group {

    public FriendsList(String name, HashMap<String, Friend> members) {
        super(name, members);
    }

    public FriendsList(String name) {
        super(name);
    }
}

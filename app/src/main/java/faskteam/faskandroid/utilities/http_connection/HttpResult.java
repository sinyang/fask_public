package faskteam.faskandroid.utilities.http_connection;


/**
 * Created by Sam.I on 10/13/2015.
 */
public interface HttpResult<T> {

    void onCallback(T response, boolean success);

}